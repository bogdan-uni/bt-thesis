\section{Introduction}\label{sec:contributions:learnComplexMobilityPatterns}



The rest of this study is formulated as follows. First, Section 2 presents a short overview of related works. We then describe in detail the proposed methodology in Section 3. The evaluation of the proposed method is presented in Section 4 followed by conclusions and future directions in Section 5.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Literature and background}
\label{sec:contributions:learnComplexMobilityPatterns}




\section{Methodology}
\label{sec:contributions:learnComplexMobilityPatterns}

The proposed methodology consists of four inter-connected components: \textit{(a)} classification of each activity using a set of aggregate statistics about activity scheduling and preferences, \textit{(b)} extraction of the weekly visit pattern for each location where respondents spent some time, \textit{(c)} heuristic process to identify home/work location and, and \textit{(d)} \Gls{gis} information to profile activities performed in all remaining locations. 

\subsection{Activity classification} \label{731}

In this section, we investigate the relationship between individual and aggregate mobility patterns. The first step consists in finding the ideal number of activities to profile. For instance, if many comparable options are considered, the model is likely to fail in distinguishing events like going to "swimming pool" and "gym". On the other hand, to consider only a limited number of activities generates an oversimplified pattern. Thus, in this section, we introduce a methodology to identify the correct number of activities to consider in the model. 

Specifically, we classify activities into two groups: rigid activities, e.g. \textit{work}, and flexible activities, like \textit{daily shopping}. It is intuitive to realize that rigid activities are highly repetitive, hence easier to detect. People always sleep at home and spend a significant deal of their working hours in their office. On the other hand, flexible activities are more difficult to detect, as they are influenced by different factors, including traffic conditions and household composition. Based on these considerations, we define the following three groups of activities: 

1)	\Gls{dsa}: Activities in which activity scheduling is not flexible (e.g., Home, Work, Education).

2)	\Gls{wsa}: Activities that are not systematic within the day but recur regularly, e.g. every week (e.g., swimming pool, weekly shopping).

3)	\Gls{nsa}: Flexible activities that represent extraordinary events with respect to the usual user activity scheduling (e.g., visiting the doctor). 

	We then propose to perform a cluster analysis to classify activities based on two variables:

	- $N_{a}^{T}$ represents how many people joined activity \textit{a
} during the reference time period \textit{T};

	- $N_{a}^{d}$ represents how many people joined activity \textit{a} at day \textit{d};

For instance, if two users go to work from Monday to Friday, then we can calculate $T = 7$ (i.e. the time period), $N_{Work}^{T}$ and $N_{Work}^{Monday}$  (i.e. we have 10 observations of the activity \textit{work} over one week, two of which on Monday). Given   $N_{a}^{T}$ and $N_{a}^{d}$, we can compute $N_{a}^{t}$  by sorting $N_{a}^{d}$  in ascending order. This means that for $t = 1$, $N_{a}^{t}$ represents the day with the lowest number of observations for activity \textit{a}, while for \textit{t = T} we will have the highest participation ratio:
\begin{equation}
\label{eq71}
{N_{a}^{t=1} \leq N_{a}^{t=2} \leq ... \leq N_{a}^{t=T-1} \leq N_{a}^{t=T}}
\end{equation}

We can then calculate the cumulative of the frequency as:
\begin{equation}
\label{eq72}
P_{a}^{t}=\frac{N_{a}^{t}}{N_{a}^{T}}
\end{equation}

We refer to $P_{a}^{t}$  as \textit{“cumulative probability”} in the rest of this paper. Figure 1 represents the ideal (theoretical) trend for \textit{“work”} and \textit{“home”} activities, given one hundred users and fourteen weeks of observations.



\begin{figure}[thpb]
	\centering
	\includegraphics[scale=0.9]{chapters/contributions/inferringUrbanMobilityHabits/figures/fig71.png}
	\caption{Cumulative representation of the probability of the activities “Home” and “Work”}
	\label{fig71}
\end{figure}

For the activity \textit{work}, it is intuitive to see that the probability can be equal to zero during weekends and uniform during the working days. Similarly, the probability to return \textit{home} is constant regardless of the day of the week (all users are likely to return home at least once each day). 
Finally, as we will show in the result section, we adopt the hierarchical clustering approach to identify the proper number of activities to be considered and to evaluate the amount of information lost as a consequence of the aggregation process. The Euclidean distance is adopted to measure the similarity between activities.

\subsection{Pattern extraction} \label{732}


The weekly activity pattern for each location visited is generated by clustering all the visit records from the beginning of the data collection period. The clustering technique is based on indexing techniques using graphs (such as ND-tree). More details about this technique can be found in \cite{22kolbe2010efficient}, \cite{23qian2006space}. The unit of analysis is one hour, resulting in a matrix with a dimension of 24 hours and seven days of the week, as we will show in the next section. Each matrix element represents the number of times a person visited a specific location. The location clustering performs the extraction based on temporal and spatial parameters: 

1)	\textit{Date range}: The time interval on which the clustering is performed. If nothing selected, the entire dataset time interval will be taken by default.

2)	\textit{Specific days and hours of the week}: Can be selected specific days and hours of the week. Locations that are visited outside the selected range will not be excluded.
 
3)	\textit{Precision}: Clustering size groups the visited points in the selected range. The precision varies from a few meters to thousands of kilometers, which can be used to profile activities which do not require high precision e.g. holiday profiling.

The profiling and classification of each location are done by computing the Euclidean distance from the visit pattern of each location with the activity matrix. The smaller Euclidean distance obtained, the higher the probability that the activity is performed in a specific location. 

The result is a probability value for each possible activity in each visited location. As both the \textit{“Activity Classification”} and \textit{“Pattern extraction”} phase are not the main focus of this paper, we refer the interested reader to \cite{8toader2018using} for further details.

 

\section{Model testing, evaluation and results}
\subsection{Clustering Analysis} \label{81}

In this section, we apply the clustering analysis introduced in section \ref{731} to two different databases. The first one is the \Gls{bmw} \cite{26viti2010analyzing}. This travel survey, collected in the region of Ghent (Belgium), contains information from 717 different individuals in the form of Travel Diaries, covering a period of three months (from 08 September 2008 to 07 December 2008). The second database is the \Gls{ms} collected at the University of Luxembourg in 2015 \cite{27sprumont2017consistency}. In this work, we use travel surveys from 52 users observed for a two weeks period (15-30 of June). Both databases have the same structure and provide the following information: departure time, arrival time, the origin of the trip, the destination of the trip, activity type, sequence of activities and mode of transport. In both cases, 12 different types of activity have been reported. Figure \ref{fig73}a shows the list of activities together with the cumulative probability for each activity in the case of the \Gls{bmw} database. Similar results have also been obtained for the \Gls{ms} database.

\begin{figure}[thpb]
	\centering
	\includegraphics[scale=1]{chapters/contributions/inferringUrbanMobilityHabits/figures/fig73.png}
	\caption{(a) Cumulative probability and list of activities for the BMW database;
		 (b) Hierarchical Cluster Analysis for the MS; 
		 (c) Hierarchical Cluster Analysis for the BMW.}
	\label{fig73}
\end{figure}

Figure \ref{fig73}a shows that, although different from those shown in Figure \ref{fig71}, activities \textit{work} and \textit{home} show expected trends. The probability for \textit{home} is almost constant over time, while for \textit{work} we see two trends, one for the weekends and one for the working days. It is also interesting to see that \Gls{dsa}, such as \textit{Walking/Riding}, have an exponential trend, meaning that observations are concentrated over a limited number of days. Finally, \textit{within-\Gls{wsa}}, such as \textit{Personal Business}, have a more linear trend. 
	
By taking into account these four fundamental shapes, we created four artificial functions, named [\Gls{dsa}-Home, \Gls{dsa}-Work, \Gls{wsa}, \Gls{dsa}] which represent the typical shape for each activity according to the classification proposed in section \ref{731}. We then performed the hierarchical cluster analysis using the Euclidean distance as the index of similarity among activities. Figure \ref{fig73}b and Figure \ref{fig73}c show the results for both the \Gls{bmw} and \Gls{ms} databases. Results support two interesting points.

First, based on the \textit{"Dissimilarity"}, in both cases, it is possible to cluster all 12 activities into four clusters, without losing too much information. Second, clusters are very similar in both cases. This was expected, as typical habits are similar in Belgium and Luxembourg. However, we can also see some major differences. Activities \textit{"eat"}, \textit{"Visit family/friends"} and \textit{"other"} belong to the \Gls{wsa}cluster in the \Gls{bmw}, while to the \Gls{nsa} in the \Gls{ms}. 

There are two possible explanations for this. First, even though similar, we are still analysing two different sample populations with different habits. The BMW represents the entire household while the \Gls{ms} represents only the University staff, including Ph.D students. It is thus possible that this sub-population eats often at the canteen of the University rather than going somewhere else. Another possible explanation is that, since the \Gls{ms} database is relatively small, there are not enough observations to properly represent these three activities. However, the cluster analysis provides in our opinion a satisfactory result in both cases, showing that activities can be aggregated and differences among different populations can be observed. In the rest of this paper, we will adopt the data from the \Gls{bmw} travel survey to profile users, as these data are statistically more representative. 


\subsection{Location profiling}


The results of the cluster analysis previously discussed are represented in Figure \ref{fig74}a (red matrices). Since the probability for \textit{home} and \textit{work} are almost constant, these matrices have been generated manually. Figure \ref{fig74}b shows the corresponding matrix for one user (information validated by the respondent). For \textit{Home} and \textit{Work}, we correctly identified the activity type, which was expected since we can see that the behaviour for these activities is very repetitive and close to the ideal one.


Concerning \textit{home} and \textit{work} location, another interesting feature is the ability to capture the activity location changing over time. Figure \ref{fig75}a shows three different home locations since the user indeed changed his home location three times during the data collection. Similar, Figure \ref{fig75}b reports two work locations, as the respondent confirmed the changing of the work location during the data collection period: the main office of the individual was relocated. The changing of activity location is a very important information that cannot be captured by the classical travel surveys, only if the survey is repeated with the same respondents at regular periods of time, something which is very expensive and difficult to implement. However, since the model can analyze data collected over many years, this limitation does not hold for the current approach.







\begin{figure}[thpb]
	\centering
	\includegraphics[scale=0.75]{chapters/contributions/inferringUrbanMobilityHabits/figures/fig75.png}
	\caption{Relocation captured for (a) Home Locations; (b) Work Locations.}
	\label{fig75}
\end{figure}





\section{Conclusions}
The main purpose of this study was to explore the possibility to classify the activities performed in frequently visited locations, without any user report or additional information.





