.

\section{Introduction}
\label{sec:contributions:modellingFramework}

%Over the years, collaborative mobility proved to be an important but challenging component of the smart cities paradigm.  In the previous chapter we presented the theoretical foundations behind the collaborative mobility. In particular, we propose a data-driven indicator for collaborative mobility that can be used by the next generation of \Gls{its} in order to match groups of people which can share transportation resources in an economical efficient way. Although the results are promising, it was not possible to test the methodology using a bigger dataset because of the complex processing operations which require new scalable methods for data analytics. 

In this chapter, we propose an innovative data-driven framework that can be used in the transportation domain with large-scale datasets and makes possible the implementation of different data science methodologies, techniques and \Gls{ml} algorithms. The main contribution of this chapter is the implementation of a framework that combines the analytical and statistical results (i.e. from the data science domain) with discrete simulations (i.e. evaluation of alternative actions), adapted for the requirements of the transportation domain and in particular for the case study of collaborative mobility.

The remainder of this chapter is organized as follows. First, Section \ref{sec:contributions:modellingFramework:motivatingCaseStudy} introduce a collaborative mobility case study, which motivates the research behind this contribution. Sections \ref{sec:contributions:modellingFramework:FeaturesMethodology} introduces the methodology behind the main concepts and features needed in the transportation domain. We thoroughly evaluate our approach in Section \ref{sec:contributions:modellingFramework:experiments}. The related work is discussed in Section \ref{sec:contributions:modellingFramework:relatedWork} before concluding in Section \ref{sec:contributions:modellingFramework:conclusions}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Motivating case study} \label{sec:contributions:modellingFramework:motivatingCaseStudy}

This study was motivated by the methodology presented in Chapter \ref{sec:indicatorMob}, which describes an indicator that can be used for assessing the \Gls{cm} between individuals. In particular, based on the value of the indicator, the authors analysed the potential for assessing collaborative services among small groups of users for different combinations of sharing services (carpooling, carsharing, shared parking). Our objective is to develop a multi-functional framework that can be used for implementing the \Gls{cm} indicator at a large scale. Moreover, we extend the study's applicability in the dynamic ridesharing domain by the framework features proposed. In the remainder of this section, we present the technical and methodological requirements that can solve the above mentioned problems, extracted from \cite{toader2017usage}.

In large scale ride-sharing systems, sensors from mobile devices and cars positioning systems are sending data continuously. Storage, management and fast access to a temporal unstructured dataset is not trivial \cite{dickinson2015fundamental}. The challenge increases when we take into consideration the complexity of underlying data models and patterns combined with the relationship between entities (people, locations, cars, personal preferences). Analysing such large historical datasets where the context and input values change over time as the entities are constantly moving calls for advanced time series analytics and technologies. While this topic has been explored by the database community \cite{segev1987logical}, it is lately gaining popularity also in the ITS domain.

Time management is a critical component in smart mobility systems. Different services make resources available simultaneously or over time. Therefore, the system must be able to analyse data over time and perform deep and complex temporal search queries. GPS location data and timestamps are received with different frequencies and sometimes data values are missing (\eg mobile phones with GPS deactivated). When \eg a request is made for a carpooling activity, filtering and searching in time and space operations must be performed, returning the possible candidates for carpooling. Often, the exact position of each rider is not exactly known and the system must return either the last known values or an extrapolation of the missing data based on the history and on specific domain rules.

Over the years, data analytics in the ITS domain shifted from \textit{descriptive} data analytics, represented by the understanding of past events, to the emergence of \textit{predictive} analytics, i.e. techniques which make predictions about the future based on learning from historical data. Therefore, different ML techniques can be implemented (\eg profiling, prediction, clustering, classification) in order to extract knowledge from unstructured data. The ML component must be able to continuously learn and the recommendation system must use the results almost in real-time. The work from Chapter \ref{sec:contributions:indicatorMob} \cite{toader2017usage} supports the idea that the large scale implementation of CM services calls for the next generation of \textit{prescriptive} analytics, capable to take efficient decisions using recommendation systems. In the ridesharing systems the current state of each entity (\eg location of users) is known from the data received, as well as the desired state expected and results (\eg grouping more users in fewer cars). Prescriptive analytics can be used to perform an what-if analysis \cite{haas2011data} by exploring different alternatives. The final goal is to give recommendations (\eg with whom an user can carpool) regarding the actions that should be done in order to reach as close as possible from the initial state to the desired state, almost in real-time (\eg shifting the departure time in order to be compatible with more users).

These are the basic requirements that the next generation of smart mobility systems must meet. This motivates our work on implementing a new modelling framework over temporal graphs that satisfies the requirements of CM recommendation systems. In the next section, the methodology behind each proposed framework features is presented, as long with the implementation details. Although in this study we use mainly a similar case study as \cite{toader2017usage}, the framework can be applied to many other domains \cite{hartmann2016near, hartmann2014reactive}.

%%%%%%%%%%%%%%%%%%%
\section{Features and methodology} \label{sec:contributions:modellingFramework:FeaturesMethodology}

This study introduces a new modelling framework that satisfies the requirements of the next generation of recommendation systems, based on the unique combination of the integrated core features. The proposed solution has the foundation in the GreyCat \cite{greycat} framework, formerly known as \Gls{kmf} \cite{francois2014kevoree}. In this section, the methodology behind each core feature is presented, together with a summary of the implementation. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Modelling with graphs} 

Graph theory has long been used in multiple domains and applications \cite{deo2017graph}. Figure \ref{graph-meta-modelling} presents the general components and the information processing flow in the case of a recommendation system that can be implemented for assessing CM using the indicator developed in \cite{toader2017usage}. The components included in the square with the dashed line are implemented and used in the current study.

\begin{figure}[thpb]
	\centering
	\includegraphics[scale=0.5]{chapters/contributions/modellingFramework/figures/modeling-through-graph.eps}
	\caption{Meta model of the graph and system components}
	\label{graph-meta-modelling}
\end{figure}

The temporal graph is used simultaneously as storage after raw data processing, as an input for the profiler component and as validation for the recommendation system. In this case, the graph stores the data in nodes represented by users, locations and context. Each node has its own attributes according to its type \eg latitude, longitude, timestamp, sequences of locations, user preferences etc. Between each node, the edges of the graph are represented by the relationships between the nodes.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Temporal aspect}

The relationships between nodes (\eg in our case study users and locations) can evolve over time. Graph structure evolution over time is presented in Figure \ref{graph-temporal-aspect}. 

\begin{figure}[thpb]
	\centering
	\includegraphics[scale=0.4]{chapters/contributions/modellingFramework/figures/graph-structure-time.eps}
	\caption{Graph structure evolution in time between users and locations}
	\label{graph-temporal-aspect}
\end{figure}

As we can observe, at each timepoint $t_n$ the graph captures the relationships between users and locations, which constantly change over time. The advantage of this type of structure is that similarities and behavioural patterns between users can be identified by comparing their evolution over time. Moreover, new users can enter the system and others leave, a typical behaviour of users in any ride sharing system.

One major advantage of intelligent systems dealing with time series data is their ability to continuously analyse their context in order to autonomously offer recommendations \cite{hartmann2014reasoning}. In the case of smart mobility systems, the dynamic context of continuously moving of users force the reasoning processes to analyse and compare the current situation with the trend created by the past events. Even if a common approach consists in a temporal discretization, this will lead to large data mining \cite{hartmann2014reasoning}. Figure \ref{time-navigation} presents the framework's concept of storing only the relevant data (data points marked with a black dot). Data represented by a cross ($\times$) is discarded for compression.

\begin{figure}[thpb]
	\centering
	\includegraphics[scale=1.6]{chapters/contributions/modellingFramework/figures/time-navigation.eps}
	\caption{Time management and irregular data frequency.}
	\label{time-navigation}
\end{figure}

In smart mobility systems, the GPS location data ($l_n$) is constantly received from $P_n$ users' smartphones. In this case, the system must manage irregular frequency data rates and optimise the storage by discarding redundant data. The proposed model will store new locations (data points marked with black dots) only if the users change their locations. If the users don't change the location, the data points (represented with $\times$) are discarded. Let us consider the situation when the system is performing a query at the time $t_{12}$. In this case, only the user $P_1$ can return the actual location. For the rest of the users, traversing back in time to the last known location is necessary to estimate the current position. In the case of missing data, different extrapolation methods can be used \cite{moawad2015polymer}. This technique allows every node to evolve independently of the other in time and greatly contributes to the compression of data storage and access speed. 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\subsection{What-if analysis exploring alternatives}

According to \cite{toader2017usage}, in order to asses the CM indicator between a group of \textit{n} riders, $(n\times (n-1))/2$ calculations of different combinations can be made to group the riders that can share a trip. Using the Quadtree indexing \cite{kothuri2002quadtree} in a graph structure has the advantage of reducing the number of calculations to $nlog(n)$. Similar, the number of steps to traverse a graph is $log(n)$, compared with $n-1$ steps in a classic flat structure. 

As \cite{haas2011data} stated, \textit{what-if} analysis is a mandatory component in this case. Our proposed framework can generate a large number of \textit{what-if scenarios}, \eg find the maximum number of riders that can carpool at a certain moment in time. Hartmann \cite{hartmann2016enabling} introduces the notion of \Glspl{mwg}, which can be defined as hypergraphs, which structure and properties can evolve along time and parallel worlds. Each simulation is done in a parallel world, used as an identifier. In Figure \ref{many-worlds-graph} is presented the general concept of \Gls{mwg}, as a solution for multiple parallel simulations. The worlds that are represented by a continuous line are resolved, while those with dashed lines are discarded as the solution provided is not fitting the objective function.

\begin{figure}[thpb]
	\centering
	\includegraphics[scale=1.6]{chapters/contributions/modellingFramework/figures/many-world-graph.eps}
	\caption{Many Worlds Graph as a solution for multiple parallel simulations}
	\label{many-worlds-graph}
\end{figure}

We define the first created world ($W_0$) as the root (parent) world, defined by the collected real data. All other worlds can be created at any time, diverging from the root world (\eg $W_1$, $W_2$, $W_3$, $W_4$, $W_8$). Then, from any other created world, other secondary worlds can occur (\eg $W_5$, $W_6$, $W_7$, $W_9$, $W_{10}$, $W_{11}$).

In the ridesharing case study, the root world is represented by the real data collected by the riders' mobile devices. When \eg the driver is asking for riders to share a trip, the framework performs multiple simulations in parallel worlds to test the indicator value for different combinations of grouped riders. If the indicator's computation shows that the riders are compatible for carpooling, the  world (\eg $W_3$) is solved as represented by a continuous line in Figure \ref{many-worlds-graph}. The scenario is then considered as successful and the recommendation system can inform the driver about the compatible rider(s). A simulation is either successfully completed and merged in $W_0$, or discarded and represented by a dashed line. If the simulation \eg $W_4$ is successful, it is also possible to perform another what-if analysis for finding an additional compatible rider for that trip. The process can be repeated until has reached the maximum capacity of the car. If $W_7$ has successfully resolved,  all the parent worlds are closed and merged.

Regarding the support for prescriptive analytics, \cite{hartmann2016enabling} showed that MWG is able to handle efficiently hundreds of thousands of independent worlds. Moreover, every node can fork independently and when forked, differential information is stored, resulting in a faster process with data compression.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Lazy loading of nodes} 
%Naturally, many analytics tasks are processing only parts of the full dataset. This also counts for the case study of this paper.
%Therefore, we suggest to load data, i.e. the nodes of our data graph only on-demand while the graph is traversed.
%To achieve this, the graph is decomposed into a set of key/value pairs, representing the graph.
%More specifically, every node value contains the attributes of a node and the relationships to other nodes (in form of sets of ids). 
%Then, a node value is stored/loaded via an identifier in/from a key/value store.
%Depending on the requirements of an application, different key/value stores can be used as backend: from simple key/value stores to scalable, replicated and distributed high-frequency stores.
%\am{this sentence has redundunt concurency and term} To ensure concurrency and fault tolerance, we use a per-node lock policy and we rely on the concrete underlying storage implementation to ensure concurrency and distribution.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\subsection{Isomorphic framework} 
%Our proposed framework allows to transpile the graph data model including all features into Java and JavaScript.
%In both target languages, the provided API is similar.
%This is called \textit{isomorphic}.
%It allows to use the same model and API to develop for example an application core in Java and the visualization, including smaller calculations, on the browser side in JavaScript.

% \bt{
% \begin{itemize}
% \item Isomorphic framework between server and client – run on the smartphone/browser
% \item Test the speed of loading of users data on the map 
% \end{itemize}
% }
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection{Additional features} 

% \begin{itemize}
% \item  Parallel computation + distribution
% \item  Solve Multi-Objective optimization problems (make it simple, the goal is just too show one search algorithm even if it brute force ,use force instance neighbourhood based on cluster results)
% \end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experiments} \label{sec:contributions:modellingFramework:experiments}

\subsection{Dataset and experimental setup}

In order to test the proposed framework, the Geolife dataset \cite{geolife-dataset} is used. This publicly available dataset contains over 25 millions GPS points, collected in the Geolife project \cite{zheng2009geolife2} from 182 users with smartphones and GPS loggers in a period of five years. There are 17,621 trajectories represented by sequences of time-stamped points, which contain the latitude, longitude and altitude with a variety of dense representation sampling rates, \eg 1-5 seconds. The characteristics of the dataset are suitable for testing the performance of real-time large scale systems and platforms. The dataset was used in many research fields, such as mobility pattern mining \cite{zheng2009mining} and study of human behaviour \cite{zheng2008understanding}.

Even if the main focus in this study is not related with the performance testing of ICT systems, one of the mandatory conditions of the real-time recommendation systems is the speed of loading, processing and data learning. Experiments are performed using an end-user laptop (Processor: quad-core 2,8 GHz Intel Core i7, RAM: 16 GB 1600 MHz DDR3, Disk: 1TB SSD). LevelDB \cite{leveldb} is used as database management system for its excellent capabilities of storage compression and querying speed. 

In order to test the above features of the framework, in the next subsections different experiments are performed. The complete source code of the implemented framework is available from \cite{playmobel}. 

\subsection{Scalability}

Data received in real-time from different sources must be processed with the following main operations: parsing, learning and saving to the database. The entire dataset was processed in 63,1 seconds with an average of 416.787 values/second. The most consuming operation was as expected the profiling (64,18\%), followed by the parsing from CSV (28,68\%) and saving in the database (7,13\%).

\subsection{Profiling}

The ML component was tested using a Gaussian Mixture Model algorithm \cite{ko2005characterization} for profiling users' location probabilities by the day of the week and hour. In total we created 336 profiles for each user, according to each day of the week at half hour interval. The results can be visualised live in the browser while the system can perform in parallel computations and update the results in asynchronous mode. In Figure \ref{user-profile-map} is presented a snapshot of the interface which can be accessed online \cite{livedemo}.

\begin{figure}[thpb]
	\centering
	\includegraphics[scale=0.5]{chapters/contributions/modellingFramework/figures/user-profile-map.png}
	\caption{User profiling probability map}
	\label{user-profile-map}
\end{figure}

By selecting any user, a day and hour of the week, the system performs a query in the database and navigates over temporal graphs at the selected timepoint. Then returns the probability map of all the visited locations, learned from the historical data. 

\subsection{Deep search and query capabilities}

The very first test was to assess how well the users dynamic movement over the 168 hours of the week was captured. 

As mentioned above, the location is stored only when the users move. As a result, the number of users that send new data in each hour of the week over the entire five years represents an indicator of the user's movement dynamics. Performing a deep search query with the average users that are moving for each day of the week, we obtained the results shown in Figure \ref{users-connected}.

\begin{figure}[thpb]
	\centering
	\includegraphics[scale=0.4]{chapters/contributions/modellingFramework/figures/user-dynamics.png}
	\caption{User movements over the weekdays}
	\label{users-connected}
\end{figure}

The observed distribution confirmed the hypotheses of the users' typical movements: the working days of the week have a similar pattern with a peak of traffic in the morning and one in the evening (specific to the commuting time period). In the weekend, the patterns are different with a maximum of movement on Saturday evening followed by very low movement on Sunday morning.

\subsection{Exploring alternatives for carpooling scenario}

In order to perform a what-if analysis experiment, we selected from the users movement distribution over the week presented in Figure \ref{users-connected} the day and the hour of the weeks the maximum number of users was moving. There are 112 users travelling around 7 pm on Saturday. 

The experiment consists on finding compatible users for a carpooling activity, using the users profiles from the learning of their five years of data. According to \cite{toader2017usage} the condition for a group of users to carpool is that the distance in time and space at departure and arrival to be minimal in order to avoid the driver to make a big detour.

The first step was to calculate the distance in time and space between the 6 most probable locations between each user, using the indicator developed in \cite{toader2017usage}. The goal is to create a score between all the users which will be used as a compatibility classifier between the selected users. In order to use a single measure unit for the distance in time and space, we transformed the geodistance in time, using an average speed of 50 km/h.

The second step is to explore all the possible alternatives for finding users compatible for carpooling, using parallel simulations in \Glspl{mwg}. The computation was done for a hypothetical departure (Saturday at 7pm) and arrival (half an hour later). Our aim is to find users that belongs to the same cluster in both times, which are then compatible for carpooling.

The cluster analysis with the results from the departure time can be visualised using clustered graphs as presented in Figure \ref{social-graph-departure}.

\begin{figure}[thpb]
	\centering
	\includegraphics[scale=0.3]{chapters/contributions/modellingFramework/figures/social-graph-departure-3.png}
	\caption{Clustering of compatible users for ridesharing departure at 7pm}
	\label{social-graph-departure}
\end{figure}

\begin{figure}[thpb]
	\centering
	\includegraphics[scale=0.3]{chapters/contributions/modellingFramework/figures/social-graph-arrival-3.png}
	\caption{Clustering of compatible users for ridesharing at 7.30pm}
	\label{social-graph-arrival}
\end{figure}

Compatible users are linked and clustered in the same group colour. For the users that are represented with the grey colour in the centre of Figure \ref{social-graph-departure}, no compatible users for carpool was found at the selected day and hour.

The same experiment is also performed half an hour later and the results are presented in Figure \ref{social-graph-arrival}. We can observe five coloured groups, represented by users that are clustered in the same group in both Figure \ref{social-graph-departure} and \ref{social-graph-arrival}.

Interestingly, the clusters evolves over time and new users can be added or removed from a group. If we look \eg at the green cluster in both figures, more users are compatible at the arrival time. This means that further analysis can be done if some of the new added users can be picked up along the trip path.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related work} \label{sec:contributions:modellingFramework:relatedWork}

Although ridesharing can provide many benefits, there are still challenges that have restricted its widespread adoption \cite{furuhata2013ridesharing}. The research community confirms that there is a need for scalable spatio-temporal stream computing frameworks that can operate big data streams, clustering and queries \cite{galic2016spatio}. There are technologies and methodologies for implementing parts of the features required for the smart mobility systems. For example, Hadoop \cite{1} and Spark \cite{zaharia2012resilient} have powerful data analytics. Graph representation and processing solutions are offered by Neo4j \cite{3} and GraphLab \cite{low2014graphlab}. What-if analysis combined with hypothetical queries are explored in different database communities \cite{balmin2000hypothetical}. Different methodologies and frameworks have been also proposed for parts of the CM requirements. Mapping and clustering have been explored using data from social media \cite{chaniotakis2016mapping}. The existing literature on activity analytics uses synthetic simulations \cite{balac2016activity}. More recent attention has focused on the human mobility patterns and predictability \cite{qian2013impact}. Our implementation offers the combination of all the above solutions in a single complete framework that can be used for CM applications.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusions} \label{sec:contributions:modellingFramework:conclusions}

We proposed a novel data modelling framework over temporal graphs that can be implemented in the ITS domain. We explained how our implementation can efficiently solve complex scenarios with multiple optimization levels of objectives in the CM applications. The presented implementation introduces, for the first time in the transportation domain, the capability to merge the discrete simulations and statistical results in a single framework. 

The results obtained from the performed experiment can be used by a future recommendation system. If the system learns the users' profiles, in the case that an user requests a ride, the search will be performed first on the cluster where the user belongs to. The advantages over classical solutions are the speed of computation and the ability to deal with the missing data which can be replaced by the highest probabilities obtained from the cluster analysis. 

The presented framework can be used in any transportation related problem that deals with large-scale datasets and ML algorithms. The algorithms library, the real-time geospatial and timeline visualisation components make the framework a complete tool for the transportation engineers. Moreover, our implementation is able to unify the functionality development phase with the implementation and final production in an single stage.

Although the results from this chapter presents a framework that can manage large-scale datasets and multiple users in the same time solving efficiently complex tasks, a collaborative mobility recommendation system requires a more user-centric approach. This is important as each user has different preferences when using shared mobility services. The contribution from the following chapter will continue the research in this direction, using the concept of users' profiling. This method is able to extract individual user's travel behaviour from raw data which can be used by a recommendation system to propose additional actions that the user can perform in order to increase the chances to find compatible users, \eg to reschedule the departure hour or position. This will increase the chances to be included in a cluster and to be compatible with other users for an eventual ride sharing.