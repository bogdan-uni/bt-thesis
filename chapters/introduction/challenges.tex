\section{Challenges}
\label{sec:intro:challenges}

%The objectives of this thesis are translated into \Glspl{rq}, which are addressed in Part II through the presented contributions. At the same time, the studies provided in each contribution raise different challenges. 
This section presents the challenges addressed in this dissertation. Each chapter discusses a challenge derived from the contribution section, pointing to the provided solutions. Each challenge corresponds to a concrete issue occurring in the transportation domain, particularly in the shared mobility topic. Very often solutions from data science domain are employed. Figures \ref{thesis-challenges} presents a flowchart with the challenges and the addressed chapters. 


\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.9]{chapters/introduction/figures/thesis-challenges}
	\caption{Challenges and contributions flowchart}
	\label{thesis-challenges}
\end{figure}

\fbox{\begin{minipage}{\textwidth}
		\textbf{Challenge \#1}:\\
		Develop a generic methodology to compute metrics that can evaluate the compatibility of people and transportation resources for different shared mobility solutions.
\end{minipage}}

Sharing travels, parking spaces and transportation resources (\ie cars, bikes) promise to be an effective way to increase the mobility resources usage rates and to reduce the number of cars and consequently road congestion. However, the literature shows many problems must yet be solved to achieve this objective, involving both operations issues (\ie how to best match users’ in time and space) and behaviour challenges (\eg specific conditions that users consider when choosing to travel together, or arrangements for accepted detour and rescheduling) \cite{viti2013equilibrium}. One of the biggest challenges is that different sharing services and solutions (\eg carpooling, parking sharing, car sharing) have different constraints, requirements and methods of finding compatible users, times and routes, all of which can satisfy all the travellers. At the same time, all the solutions found must reduce the cost and/or time of travel, at the level of both the individual and the exploitation system. Solutions must also offer a degree of flexibility in order to maintain the mobility service quality level and user satisfaction.

In order to respond to this challenge, the contribution from Chapter \ref{sec:indicatorMob} is the introduction of an indicator that can be successfully used to assess if different users are compatible and whether is economically advantageous to share the ride or parking space for both the short or long term. The proposed method is designed in such a way that not only takes into consideration all the variables (\eg schedule, personal preferences, flexibility) and possible costs (\eg travel cost, parking fee), but also provides an implementation usable by future \Gls{ml} based \Gls{rs}.

\fbox{\begin{minipage}{\textwidth}
\textbf{Challenge \#2}:\\ 
Implement a framework which can manage the distributed, dynamic and heterogeneous big data from smart mobility and analyse data in motion, while exploring the hypothetical actions of recommendation systems.
\end{minipage}}

One of the biggest challenges in the smart mobility domain is the use of data science as an enabler for implementation of large scale transportation sharing solutions. In particular, the next generation of \Gls{its} requires the combination of \Gls{ml}, \Gls{ai} and discrete simulations when exploring the effects of what-if decisions in complex scenarios with millions of users. This challenge is addressed in Chapter \ref{sec:modellingFrameworkIntro}, which develops a multi-functional framework that can satisfy the requirements of descriptive and predictive analytics in real-world shared mobility scenarios (\ie carpooling, car sharing, parking sharing). We demonstrate that the proposed framework is able to handle massive amounts of continuously changing data coming from data in motion. Moreover, the proposed methodology is capable of merging the discrete simulations and statistical results in a single framework in a fast, efficient and complete architecture that can be easily deployed, tested and used.  

\fbox{\begin{minipage}{\textwidth}
		\textbf{Challenge \#3}:\\ 
		Develop a method capable of dynamically profiling users and all the visited locations in order to extract knowledge from raw data (\ie travel patterns and habits) that can be used for the implementation of shared mobility solutions. 
\end{minipage}}

Statistical methods are widely used in the transportation domain to extract commonalities and similar behaviours for a large number of users \cite{viti2010analyzing}, \cite{sprumont2016considering}. However, providing personalised shared mobility solutions for each individual using a common behavioural model cannot provide all the time satisfactory results. This challenge is addressed in Chapter \ref{sec:dynamicProfilingIntro}, where, for the first time, a method is proposed for dynamic profiling of users and visited locations. The resultant profiling method is used to extract insights, travel patterns and habits, using only the \Gls{gps} data collected from nomadic and wearable devices, without any travel survey or user input. Using this method, valuable knowledge regarding travel habits can be discovered, which can be used for recommendation systems to match people and shared mobility services in an autonomous, fast and dynamic way. 

\fbox{\begin{minipage}{\textwidth}
		\textbf{Challenge \#4}:\\ 
		Integrate data science and behavioural methods to learn complex mobility patterns and travel habits while performing advanced analytics automatically, without any respondent's input.
\end{minipage}}

Until now, extraction of daily and weekly activity/travel patterns and habits has been done using manually user reported information, coupled with \Gls{gps} loggers that record the movement automatically \cite{stopher2010conducting}, or the collection of data using smartphones \cite{allstrom2017smartphone}. However, all these methods have required a certain degree of user input in order to extract knowledge from raw  data and to reason about the travel habits and patterns of each individual. The result: an expensive data collection process resulting in less data collected, due to a lower number of respondents willing to engage in this activity. Although a completely independent data collection and analysis method could be a solution, this represents a big challenge, as the data collected has no semantics interpretation or meaning in this case. This challenge is addressed in Chapter \ref{sec:learnComplexMobilityPatternsIntro}, where practical examples of learning complex mobility patterns and inferring urban mobility and habits are presented. Therefore, complex analytic and reasoning actions were performed automatically, without any user intervention \eg classification of the activity performed in each location and reconstruction of complex mobility patterns. We demonstrate also that the use of external contextual data from \Gls{gis} information coupled with different rules can improve the overall accuracy of the proposed model.
