%-----------------------------------------------------%
% @author: Tony Beltramelli - www.tonybeltramelli.com %
%       IT University of Copenhagen - www.itu.dk      %
%       creation date: 2015/06/11, version: 0.1       %
%       last update: 2015/12/05, version: 0.2         %
%-----------------------------------------------------%
% @author: Thomas Hartmann - http://wwwde.uni.lu/snt/people/thomas_hartmann %
%       University of Luxembourg - http://wwwen.uni.lu/      %
%       last update: 2016/03/10, version: 0.3         %
%-----------------------------------------------------%
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{phdthesis}[2016/03/08 v0.1 PhD thesis]
\LoadClass[12pt, twoside, a4paper]{book}

\RequirePackage[english]{babel}
\RequirePackage[utf8]{inputenc}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage{amssymb}
\RequirePackage{graphicx}
\RequirePackage{float}
\RequirePackage{verbatim}
\RequirePackage{subcaption}
\RequirePackage{caption}
\RequirePackage{listings}
\RequirePackage{url}
\RequirePackage{comment}
%\RequirePackage[toc,page]{appendix}


\RequirePackage[dvipsnames]{xcolor}
\definecolor{special}{rgb}{0.3, 0.3, 0.3}

\RequirePackage{emptypage}
\RequirePackage{rotating}
\RequirePackage{hyperref}
\hypersetup{
	linktocpage,
   colorlinks,
    citecolor=special,
    filecolor=black,
    linkcolor=black,
    urlcolor=special,
}

\RequirePackage{nonumonpart} % removes the page numbers from the part title pages

\RequirePackage{pdfpages}

\renewcommand\bibname{references}

%\RequirePackage{tocloft}
%\renewcommand\cftpartfont{\large\bfseries} % redefines the font size of parts in the table of contents

\RequirePackage{pdfsync}
\synctex=1

\RequirePackage[times]{quotchap}
\definecolor{chaptergrey}{rgb}{0.5,0.5,0.5}

\RequirePackage{setspace} 
%\onehalfspacing

\RequirePackage{multirow}

% geometry definition
\RequirePackage[left=3.0cm,right=2.55cm,top=1.8cm,bottom=1.0cm,includefoot,includehead,headheight=15pt]{geometry}

%\parindent 12pt % indent for new paragraphs
\parindent 0pt % indent for new paragraphs
\parskip 1em

\RequirePackage{fancyhdr}
\pagestyle{fancy}                       % Sets fancy header and footer
\fancyfoot{}                            % Delete current footer settings
\fancyhead{}

\fancyhead[LE]{\textit{\nouppercase{\leftmark}}}      % Chapter in the right on even pages
\fancyhead[RO]{\textit{\nouppercase{\rightmark}}}     % Section in the left on odd pages
\fancyfoot[RO, LE]{\thepage}
\let\headruleORIG\headrule
\renewcommand{\headrule}{\color{black} \headruleORIG}
\renewcommand{\headrulewidth}{0.5pt}

\fancypagestyle{plain}{ %  
\fancyhf{}
\fancyfoot[RO, LE]{\thepage}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
}

\RequirePackage{bibentry}
\nobibliography*


\RequirePackage[acronym,toc]{glossaries}
\input{frontmatter/abbreviations}
\makeglossaries

\RequirePackage{bookmark}

\RequirePackage[nohints]{minitoc}

\setcounter{secnumdepth}{3}
\setcounter{tocdepth}{3}

\newcommand{\abstractpage}{
	\newpage
	\chapter*{Abstract}
	\input{frontmatter/abstract}
}

\newcommand{\acknowledgments}{
    \newpage
	\chapter*{Acknowledgments}
	\input{frontmatter/acknowledgments}
}

\newcommand{\contents}{
    \newpage
    \setlength{\parskip}{0pt}

	\dominitoc
    \tableofcontents
}

\newcommand{\listfigures}{
    \newpage
    \renewcommand{\listfigurename}{List of figures}
    \addcontentsline{toc}{chapter}{List of figures}
    \listoffigures
}


\newcommand{\listtables}{
    \newpage
	\renewcommand{\listtablename}{List of tables}
    \addcontentsline{toc}{chapter}{List of tables}
    \setlength{\parskip}{1em}
    \listoftables
}

%\newcommand{\listalgorithms}{ 
%   \newpage
%	\renewcommand{\listalgorithmname}{List of algorithms and listings}
%    \addcontentsline{toc}{chapter}{List of algorithms and listings}
%    \setlength{\parskip}{1em}
%    \listofalgorithms
%}

\newcommand{\chapterheadline}[1]{
    \renewcommand\chapterheadstartvskip{\vspace*{#1\baselineskip}} 
}

\newcommand{\makebibliography}{
    \newpage
    \chapterheadline{-3}
    \addcontentsline{toc}{chapter}{Bibliography}
    \bibliography{references}
    \bibliographystyle{plain}
    
    \chapterheadline{-8}
}

\newcommand{\makefrontmatter}{
    \setcounter{page}{1}
    \pagenumbering{roman}

    \chapterheadline{-3}
    
    \abstractpage
    \acknowledgments
    \contents
   	\printglossary[title={List of abbreviations and acronyms}]  
    \listfigures
    \listtables
	\listalgorithms
	
    \cleardoublepage
    
    \setcounter{page}{1}
    \pagenumbering{arabic}
    
    \chapterheadline{-8}
    
}

\newenvironment{vcentrepage}
{\newpage\vspace*{\fill}\thispagestyle{empty}\renewcommand{\headrulewidth}{0pt}}
{\vspace*{\fill}}
